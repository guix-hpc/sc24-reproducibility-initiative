;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(use-modules (gnu)
             ((gnu services) #:select (gc-root-service-type))
             (guix channels) (guix grafts))
(use-service-modules networking ssh)
(use-package-modules certs commencement
                     emacs maths mpi package-management python-web
                     screen ssh tmux version-control vim)

(define hpc-channels
  ;; HPC channels made available within the image.
  (list (channel
         (name 'guix-hpc)
         (url "https://gitlab.inria.fr/guix-hpc/guix-hpc.git")
         (branch "master")
         (commit "03d387bdee4add25ccb7ef12587e710c134d780b"))
        (channel
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
         (branch "master")
         (commit
          "40f53e8fb5b867e3a1e8fa798328423718282aac")
         (introduction
          (make-channel-introduction
           "9edb3f66fd807b096b48283debdcddccfea34bad"
           (openpgp-fingerprint
            "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA"))))))

(define (unpin-channel ch)
  "Ensure CH, a channel, is not pinned to a specific commit."
  (channel (inherit ch) (commit #f)))

(define %authorized-guix-keys
  ;; Authorized signing keys for substitutes (pre-built binaries).
  (append (list (local-file "guix.bordeaux.inria.fr.pub"))
          %default-authorized-guix-keys))

(define %ccadmin-ssh-key
  ;; SSH public key of the 'ccadmin' user specified in
  ;; <https://github.com/ChameleonCloud/CC-Images/blob/main/ImageDefs.md#default-user-accounts>.
  (plain-file "ccadmin.pub"
              "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDIP5yCb+pms83A9f0JPDx+ILOAYy7OoxPf5MjZlL+JW/bWRKuMmQDYLi6F22QoFlRrL0LsNsR8m28F/4uox81QOVq6D+uXODUe0FLJF1SLowxVrhvtEQSb0p2Ep3F3Isk6niEVTYNz+Uyx1s5jZ3xF1CjUFt8fhggkZEn+mAxux8FQO35mJF0mbEVNAA/ZiApLmM8/oBxtOd9T3QHEoXD5wensAWlGk2HdQVUMHNFRLZPl3Oo4BHYI3CbD/mu6dOZLOF/bvJF6dnwmQI+JflSLHLwGObKdi96w+sOEQfuYA1hMCes4M7rTkrNwAoLJUxZjr/l1/EgJJbbEIGiKOUW4TtNTiaIfV58ynhKdlvd7sIEquWGnPTtz3qviSkEmPiub48mt99sMXbHCNOFTz4dIwtjC2RQEWbRbccdVYRYBXXM9p5lThcRiyWrgkneIi/wfaRCKXzpzhSREgWWrFQ6/8p0ZbLeT/UGP6B0Lo7u1MsTSGa54VzK8AfhYJLMXJH/V+59g0iMN/owcUknI6KdLrsP9TW9jB7mKwtHBHyxC4IGcEKwwT+rW1hyz3JfHr0GEzt4FW86Ryyv2nb4yB4RC/R8YkWOM9mSJsQbZkAmXVWVJLCzE36cH0SgyFZvYfW2O7Ly+pwnVIgoPfowRTHCxCyO07qI+S22WLEH1HNw98w== Chameleon admin access"))

(define sc24-message-of-the-day
  (plain-file "motd" "
\x1b[1;37mWelcome to the SC24 Guix System image.\x1b[0m

\x1b[2m\
This is a bare-bones GNU/Linux system, but you can extend the set
of available packages by running 'guix install'.  You can also create
one-off development environments using 'guix shell'.

Check out https://guix.gnu.org/guix-refcard.pdf for a quick overview
of the available commands or run 'guix help'.\x1b[0m
"))

(define commonly-used-hpc-packages
  ;; Packages commonly used in HPC that the image should already include as a
  ;; way to reduce the need to re-download or re-build software.
  (list gcc-toolchain-11 gnu-make openmpi openblas))

(define (ungrafted p)
  "Return an ungrafted variant of P."
  (with-parameters ((%graft? #f))
    p))


;; Operating system definition.
(operating-system
  (host-name "sc24repro")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/sdX"))))

  (file-systems (cons (file-system
                        (device (file-system-label "my-root"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; Accounts and groups as specified in
  ;; <https://github.com/ChameleonCloud/CC-Images/blob/main/ImageDefs.md#default-user-accounts>.

  (users (append (list (user-account
                        (name "cc")
                        (uid 1000)
                        (comment
                         "SC24 Reproducibility Initiative Chameleon Guest")
                        (group "cc")
                        (supplementary-groups '("users" "wheel"))
                        (password ""))
                       (user-account
                        (name "ccadmin")
                        (uid 1010)
                        (comment
                         "SC24 Reproducibility Initiative Chameleon Admin")
                        (group "ccadmin")
                        (supplementary-groups '("users" "wheel"))
                        (password "")))
                 %base-user-accounts))

  (groups (append (list (user-group (name "cc") (id 1000))
                        (user-group (name "ccadmin") (id 1010)))
                  %base-groups))

  (sudoers-file
   ;; Default /etc/sudoers contents: 'root' and all members of the 'wheel'
   ;; group can do anything.  'cc' and 'ccadmin' are sudoers with no
   ;; password.
   (plain-file "sudoers" "\
root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL
cc ALL=(ALL) NOPASSWD:ALL
ccadmin ALL=(ALL) NOPASSWD:ALL\n"))

  ;; Globally-installed packages.
  (packages (append (list nss-certs screen tmux vim emacs-minimal
                          git-minimal python-cloud-init)
                    %base-packages))

  ;; Add services to the baseline.
  (services (append (list (service dhcp-client-service-type)
                          (service openssh-service-type
                                   (openssh-configuration
                                    (openssh openssh-sans-x)
                                    (password-authentication? #f)
                                    (authorized-keys
                                     `(("ccadmin" ,%ccadmin-ssh-key)))))
                          (service ntp-service-type)

                          (service gc-root-service-type
                                   (map ungrafted commonly-used-hpc-packages)))

                    (modify-services %base-services
                      (login-service-type
                       config => (login-configuration
                                  (inherit config)
                                  (motd sc24-message-of-the-day)))

                      ;; Automatic login on the serial console.
                      (agetty-service-type
                       config =>  (agetty-configuration
                                   (baud-rate "115200,38400,9600")
                                   (auto-login "cc")
                                   (no-clear? #t)
                                   (tty #f)       ;automatic
                                   (shepherd-requirement '(syslogd))))

                      (guix-service-type
                       config => (guix-configuration
                                  (inherit config)

                                  ;; Specify the channels available by
                                  ;; default and those that appear in
                                  ;; /etc/guix/channels.scm.
                                  (channels (map unpin-channel hpc-channels))
                                  (guix (guix-for-channels hpc-channels))

                                  ;; Add guix.bordeaux.inria.fr to the set of
                                  ;; authorized substitute servers.
                                  (authorized-keys %authorized-guix-keys)
                                  (substitute-urls
                                   '("https://ci.guix.gnu.org"
                                     "https://bordeaux.guix.gnu.org"
                                     "https://guix.bordeaux.inria.fr"))))))))
